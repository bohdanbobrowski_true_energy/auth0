"""Python Flask WebApp Auth0 integration example
"""
import asyncio
import json
import urllib
from os import environ as env
from urllib.parse import quote_plus, urlencode

import aiohttp as aiohttp
from authlib.integrations.flask_client import OAuth
from dotenv import find_dotenv, load_dotenv
from flask import Flask, redirect, render_template, session, url_for

AUTH0_DOMAIN = env.get("AUTH0_DOMAIN")
ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)

app = Flask(__name__)
app.secret_key = env.get("APP_SECRET_KEY")


oauth = OAuth(app)

oauth.register(
    "auth0",
    client_id=env.get("AUTH0_CLIENT_ID"),
    client_secret=env.get("AUTH0_CLIENT_SECRET"),
    client_kwargs={
        "scope": "openid profile email",
    },
    server_metadata_url=f'https://{env.get("AUTH0_DOMAIN")}/.well-known/openid-configuration',
)


# Controllers API
@app.route("/")
def home():
    return render_template(
        "home.html",
        session=session.get("user"),
        pretty=json.dumps(session.get("user"), indent=4),
    )


@app.route("/callback", methods=["GET", "POST"])
def callback():
    token = oauth.auth0.authorize_access_token()
    session["user"] = token
    return redirect("/")


@app.route("/login")
def login():
    return oauth.auth0.authorize_redirect(
        redirect_uri=url_for("callback", _external=True)
    )


async def management_api_request(
    method: str, url: str = "", access_token: str = None, payload: dict = None
) -> aiohttp.ClientResponse:
    api_url = urllib.parse.urljoin(f"https://{AUTH0_DOMAIN}", url)
    http_headers = {"content-type": "application/json"}
    if access_token:
        http_headers["authorization"] = f"Bearer {access_token}"
    async with aiohttp.ClientSession(headers=http_headers) as aio_session:
        if method == "POST":
            response = await aio_session.post(api_url, data=json.dumps(payload))
        if method == "DELETE":
            response = await aio_session.delete(api_url)
        await aio_session.close()
    return response


async def management_api_get_token() -> str:
    payload = {
        "client_id": env.get("AUTH0_CLIENT_ID"),
        "client_secret": env.get("AUTH0_CLIENT_SECRET"),
        "audience": f"https://{AUTH0_DOMAIN}/api/v2/",
        "grant_type": "client_credentials",
    }
    resp = await management_api_request("POST", "/oauth/token", payload=payload)
    json_response = await resp.json()
    access_token = json_response.get("access_token")
    return access_token


@app.route("/logout")
def logout():
    session.clear()
    return redirect(
        "https://"
        + env.get("AUTH0_DOMAIN")
        + "/v2/logout?"
        + urlencode(
            {
                "returnTo": url_for("home", _external=True),
                "client_id": env.get("AUTH0_CLIENT_ID"),
            },
            quote_via=quote_plus,
        )
    )


async def revoke_action(access_token: str) -> dict:
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    url = f"https://{AUTH0_DOMAIN}/oauth/revoke"
    async with aiohttp.ClientSession(
        headers={"content-type": "application/json"}
    ) as sess:
        response = sess.post(
            url,
            data=json.dumps(
                {
                    "client_id": env.get("AUTH0_CLIENT_ID"),
                    "client_secret": env.get("AUTH0_CLIENT_SECRET"),
                    "token": access_token,
                }
            ),
        )
        sess.close()
    return {"response": response.text, "status_code": response.status_code}


@app.route("/revoke/<access_token>")
def revoke(access_token: str):
    """This endpoint was only about revoking token
    Documentation:
    - https://auth0.com/docs/secure/tokens/refresh-tokens/revoke-refresh-tokens
    - https://developer.apple.com/documentation/sign_in_with_apple/revoke_tokens

    :param access_token:
    :return:
    """
    response = revoke_action(access_token)
    return response
    return redirect("/")


@app.route("/delete/<user_id>")
async def delete(user_id: str):
    access_token = await management_api_get_token()
    res = await management_api_request("DELETE", f"/api/v2/users/{user_id}", access_token)
    return {
        "res": res.status,
        "access_token": access_token,
    }


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=env.get("PORT", 8000), debug=True)
